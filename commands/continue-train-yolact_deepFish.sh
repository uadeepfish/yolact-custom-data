source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
## iteration must be the last one it was on the weight file
cd ".."
python train.py --config=yolact_resnet50_DeepFish_config --batch_size=16 --resume=weights/df.pth --start_iter=10000 --save_interval=1000 --num_workers=3 >> "logs/log_train.txt" 2>&1
