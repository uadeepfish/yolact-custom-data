source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
cd ".."
python train.py --config=yolact_resnet50_DeepFish_config --batch_size=16 --save_interval=100000 --num_workers=4 > "logs/log_train.txt" 2>&1 
