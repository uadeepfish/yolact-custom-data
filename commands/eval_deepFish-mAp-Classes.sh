source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
cd ".."
rm "logs/mAP-Classes.txt"
python3 eval.py --trained_model=weights/df.pth --config=yolact_resnet50_DeepFish_config --eval_per_classes=True --conf_matrix=True #> "logs/mAP-Classes.txt"
#cd "logs"
#python3 getGraphClassesMAP.py

