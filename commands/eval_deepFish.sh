source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
cd ".."
rm -R output_images
python3 eval.py --trained_model=weights/df.pth --config=yolact_resnet50_DeepFish_config --score_threshold=0.6 --top_k=20 --images=dataset/DeepFish/test_images:output_images
files=(output_images/*)
eog ${files[0]}
