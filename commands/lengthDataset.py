

import torchvision.datasets as dset
path = "~/Desktop/yolact-custom-data/dataset/DeepFish/val/images/"
annPath = "/home/deep/Desktop/yolact-custom-data/dataset/DeepFish/val/data_fish.json"
coco_train = dset.CocoDetection(root = path, annFile = annPath)
print('Number of samples: ', len(coco_train))
