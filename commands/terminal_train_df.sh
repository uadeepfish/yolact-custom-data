source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
cd ".."
python train.py --config=yolact_resnet50_DeepFish_config --batch_size=16
