source ~/anaconda3/etc/profile.d/conda.sh
conda activate yolact-env
cd ".."
rm -R videos/output_video.mp4

# Display a video in real-time. "--video_multiframe" will process that many frames at once for improved performance.
# If you want, use "--display_fps" to draw the FPS directly on the frame.
#python eval.py --trained_model=weights/df.pth --config=yolact_resnet50_DeepFish_config --score_threshold=0.15 --top_k=15 --video_multiframe=4 --video=videos/Testvideo.mp4 --display_fps

# Display a webcam feed in real-time. If you have multiple webcams pass the index of the webcam you want instead of 0.
#python eval.py --trained_model=weights/df.pth --config=yolact_resnet50_DeepFish_config --score_threshold=0.15 --top_k=15 --video_multiframe=4 --video=0

# Process a video and save it to another file. This uses the same pipeline as the ones above now, so it's fast!
python eval.py --trained_model=weights/df.pth --config=yolact_resnet50_DeepFish_config --score_threshold=0.15 --top_k=15 --video_multiframe=4 --video=videos/lonja3.mp4:videos/output_video.mp4 --display_fps



