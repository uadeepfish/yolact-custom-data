# Print graph from table

import re, sys, os
import matplotlib.pyplot as plt

# A mano, cantidad de veces que sale cada especie en el dataset. Datos de "Acotadas" 800/180 con 57 clases
# incidencias = {"Pagrus pagrus": 427 ,"Mullus surmulentus": 1198 ,"Pagellus erythrinus": 416 ,"Sphyraena sphyraena": 105 ,"Spicara maena": 260 ,"Pagellus acarne": 751 ,"Sarda sarda": 21 ,"Scorpaena porcus": 114 ,"Spondyliosoma cantharus": 6 ,"Diplodus vulgaris": 33 ,"Diplodus anularis": 552 ,"Symphodus tinca M": 107 ,"Symphodus tinca H": 164 ,"Serranus scriba": 535 ,"Scorpaena notata": 18 ,"Labrus merula": 7 ,"Mullus barbatus": 448 ,"Dactylopterus volitans": 3 ,"Species 50": 7 ,"Merlucius merlucius": 54 ,"Conger conger": 2 ,"Sarpa salpa": 4 ,"Symphodus mediterraneus": 12 ,"Serranus cabrilla": 23 ,"Scorpaena scrofa": 3 ,"Uranoscopus scaber": 4 ,"Diplodus sargus": 55 ,"Species 52": 10 ,"Lithognathus mormyrus": 11 ,"Sepia officinalis": 188 ,"Seriola dumerilií": 3 ,"Raja asterias": 2 ,"Raja radula": 6 ,"Zeus faber": 1 ,"Dentex dentex": 54 ,"Trachurus mediterraneus": 3 ,"Torpedo marmorata": 2 ,"Sparus aurata": 16 ,"Chelidonichthys lastoviza": 6 ,"Species 51": 1 ,"Sardinella aurita": 6 ,"Muraena helena": 12 ,"Scomber japonicus": 8 ,"Microchirus ocellatus": 1 ,"Phycis phycis": 6 ,"Umbrina cirrosa": 8 ,"Sciaena umbra": 4 ,"Trachinus draco": 1 ,"Pomadasys incisus": 3 ,"Boops boops": 3 ,"Labrus viridis": 1 ,"Lophius sp": 3 ,"Spicara smaris": 2 ,"Mustelus mustelus": 1 ,"Balistes capriscus": 1 ,"Pomatomus saltatrix": 3 ,"Species 68": 1 ,"Species 54": 1}
# 13 clases reducidas
incidencias = {"Pagrus pagrus": 428 ,"Mullus surmulentus": 1198 ,"Pagellus erythrinus": 422 ,"Sphyraena sphyraena": 105 ,"Spicara maena": 260 ,"Pagellus acarne": 751 ,"Scorpaena porcus": 114 ,"Diplodus anularis": 552 ,"Symphodus tinca M": 107 ,"Symphodus tinca H": 164 ,"Serranus scriba": 535 ,"Mullus barbatus": 448 ,"Sepia officinalis": 188}
incidenciasRepartidas = {'Pagrus pagrus': {'train': 385, 'val': 43}, 'Mullus surmulentus': {'train': 1078, 'val': 120}, 'Pagellus erythrinus': {'train': 379, 'val': 43}, 'Sphyraena sphyraena': {'train': 94, 'val': 11}, 'Spicara maena': {'train': 234, 'val': 26}, 'Pagellus acarne': {'train': 675, 'val': 76}, 'Scorpaena porcus': {'train': 102, 'val': 12}, 'Diplodus anularis': {'train': 496, 'val': 56}, 'Symphodus tinca M': {'train': 96, 'val': 11}, 'Symphodus tinca H': {'train': 147, 'val': 17}, 'Serranus scriba': {'train': 481, 'val': 54}, 'Mullus barbatus': {'train': 403, 'val': 45}, 'Sepia officinalis': {'train': 169, 'val': 19}}
# Balanceado de datos
#incidenciasRepartidas = {'Pagrus pagrus': {'train': 112, 'val': 29}, 'Mullus surmulentus': {'train': 122, 'val': 19}, 'Pagellus erythrinus': {'train': 113, 'val': 28}, 'Sphyraena sphyraena': {'train': 128, 'val': 13}, 'Spicara maena': {'train': 128, 'val': 13}, 'Pagellus acarne': {'train': 126, 'val': 15}, 'Scorpaena porcus': {'train': 133, 'val': 8}, 'Diplodus anularis': {'train': 130, 'val': 11}, 'Symphodus tinca M': {'train': 131, 'val': 10}, 'Symphodus tinca H': {'train': 131, 'val': 10}, 'Serranus scriba': {'train': 130, 'val': 11}, 'Mullus barbatus': {'train': 132, 'val': 9}, 'Sepia officinalis': {'train': 132, 'val': 9}}

# 10 clases julio
#incidencias = {"Pagrus pagrus": 585 ,"Spicara maena": 636 ,"Mullus surmulentus": 1245 ,"Pagellus erythrinus": 637 ,"Pagellus acarne": 901 ,"Diplodus anularis": 793 ,"Serranus scriba": 1104 ,"Symphodus tinca H": 335 ,"Mullus barbatus": 546 ,"Sepia officinalis": 265}
#incidenciasRepartidas = {'Pagrus pagrus': {'train': 149, 'val': 38}, 'Spicara maena': {'train': 83, 'val': 21}, 'Mullus surmulentus': {'train': 79, 'val': 20}, 'Pagellus erythrinus': {'train': 164, 'val': 41}, 'Pagellus acarne': {'train': 84, 'val': 21}, 'Diplodus anularis': {'train': 72, 'val': 19}, 'Serranus scriba': {'train': 83, 'val': 21}, 'Symphodus tinca H': {'train': 79, 'val': 20}, 'Mullus barbatus': {'train': 45, 'val': 12}, 'Sepia officinalis': {'train': 46, 'val': 12}}

#5 clases mas instancias julio
#incidencias = {"Mullus surmulentus": 1245, "Pagellus erythrinus": 637, "Pagellus acarne": 901, "Diplodus anularis": 793, "Serranus scriba": 1104}

#5 clases mas accuracy julio
#incidencias = {"Pagrus pagrus": 585, "Mullus surmulentus": 1245, "Diplodus anularis": 793, "Symphodus tinca H": 335, "Mullus barbatus": 546}
#incidenciasRepartidas = {'Pagrus pagrus': {'train': 149, 'val': 38}, 'Mullus surmulentus': {'train': 79, 'val': 20}, 'Diplodus anularis': {'train': 72, 'val': 19}, 'Symphodus tinca H': {'train': 79, 'val': 20}, 'Mullus barbatus': {'train': 45, 'val': 12}}

with open("mAP-Classes.txt", 'r') as f:
    inp = f.read()

patterns = {
    'total': re.compile(r'^#################### All Classes  ___ NumAll:\s(?P<count>[0-9]+)\s#+'),
    'classes': re.compile(r'#+\s+[A-z]+:\s(?P<class>[^-]+)-+\s[A-z]+:\s(?P<count>[0-9]+)\s#+'),
    'val': re.compile(r'\s*(?P<type>[a-z]+) \|\s*(?P<all>\S+)')
}
data = {key: [] for key in patterns}
subPlots = 8
sizeLabels = 9
totalData = 0

def getTotalPerc():
    global totalData
    totalData = 0
    for dat in incidencias:
        totalData += incidencias[dat]


def getPerc(xclass):
    notSpaceClass = xclass
    if xclass[-1] == ' ':
        notSpaceClass = xclass[0:-1]

    percF = float(incidencias[notSpaceClass])
    perc = float(percF / totalData)
    perc = round(perc*100, 2)
    return str(perc)


def printTableAllThresholds(subData):
    tab = "\t"
    sp, box, masks, thresh = [], [], [], []
    for x in subData:
        if x['type'] == 'box':
            sp.append(x['class'])
            box.append(x['all'])
            notSpaceClass = x['class']
            if x['class'][-1] == ' ':
                notSpaceClass = x['class'][0:-1]
            thresh.append(x['thresh'])
        if x['type'] == 'mask':
            masks.append(x['all'])

    print("Specie" + tab + "Box" + tab + "Mask" + tab + "Threshold" )
    for i in range(len(sp)):
        print(str(sp[i]) + tab + str(box[i]) + tab + str(masks[i]) + tab + str(thresh[i]))




def printTable(subData):
    tab = "\t"
    h0 = ["Specie", "Box", "Mask"]
    sp, box, masks, perc, ninst, inc = [], [], [], [], [], []
    for x in subData:
        if x['type'] == 'box':
            #sp.append(x['class'] + ' ' + getPerc(x['class']) + '%')
            sp.append(x['class'])
            box.append(x['all'])
            notSpaceClass = x['class']
            if x['class'][-1] == ' ':
                notSpaceClass = x['class'][0:-1]
            perc.append(getPerc(x['class']))
            ninst.append(incidencias[notSpaceClass])
            inc.append(incidenciasRepartidas[notSpaceClass])
        if x['type'] == 'mask':
            masks.append(x['all'])

    #from beautifultable import BeautifulTable

    #table = BeautifulTable()
    # table.column_headers = h0

    print("Specie" + tab + "Box" + tab + "Mask" + tab + "Perc" + tab + "Num.Instances" + tab + "Num.Train" + tab + "Num.Val")
    for i in range(len(sp)):
        hx = [str(sp[i]), str(box[i]), str(masks[i])]
        # table.append_row(hx)
        print(str(sp[i]) + tab + str(box[i]) + tab + str(masks[i]) + tab + str(perc[i]) + tab + str(ninst[i]) + tab + str(inc[i]["train"]) + tab + str(inc[i]["val"]))

    #print(table)


def plot_sub(subData, index, fig, ax):
    x = [x['class'] + ' ' + getPerc(x['class']) + '%' for x in subData if x['type'] == 'box']
    ax.plot(x, [x['all'] for x in subData if x['type'] == 'box'])
    ax.plot(x, [x['all'] for x in subData if x['type'] == 'mask'])
    ax.legend(['BBox mAP', 'Mask mAP'])
    # plt.show()
    plt.savefig("../results/mAPClasses/" + str(index) + ".png", dpi=100)


def plot_eval(dat):
    plt.xlabel('Specie')
    plt.ylabel('mAP')

    half = len(dat) / subPlots
    half = half if (len(dat) % len(data) / 2 == 0) else half + 1
    orihalf = half

    subArrs = {}
    counterSublists = 0
    subArrs[counterSublists] = []
    for i in range(len(dat)):
        if i < half:
            subArrs[counterSublists].append(dat[i])
        elif i >= half and dat[i]['type'] == 'mask':
            subArrs[counterSublists].append(dat[i])
        else:
            counterSublists += 1
            half = half + orihalf
            subArrs[counterSublists] = []
            subArrs[counterSublists].append(dat[i])

    for subA in subArrs:
        fig, ax = plt.subplots(figsize=(20, 8))
        # fig.title(os.path.basename(' Eval mAP ' + str(subA))
        for label in (ax.get_xticklabels() + ax.get_yticklabels()):
            label.set_fontsize(sizeLabels)
        plot_sub(subArrs[subA], subA, fig, ax)

for line in inp.split('\n'):
    for key, pattern in patterns.items():
        f = pattern.search(line)

        if f is not None:
            datum = f.groupdict()

            if key == 'total':
                totalData = float(datum['count'])
                continue
            for k, v in datum.items():
                if v is not None:
                    try:
                        v = float(v)
                    except ValueError:
                        pass
                    datum[k] = v
            if key == 'val':
                clase = data['classes'][-1]
                datum['class'] = clase['class']
                datum['count'] = clase['count']
            if key == 'thresh':
                clase = data['classes'][-1]

            data[key].append(datum)
            break

os.system("rm -f ../results/mAPClasses/*")
getTotalPerc()
# plot_eval(data['val'])
printTable(data['val'])
# printTableAllThresholds(data['val'])