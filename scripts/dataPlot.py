## Plot loss por defecto funciona, no es necesario este fichero

import os, sys
import matplotlib.pyplot as plt

sys.path.append('/home/deep/Desktop/yolact-custom-data/utils')

from functions import MovingAverage


def smoother(y, interval=100):
	avg = MovingAverage(interval)

	for i in range(len(y)):
		avg.append(y[i])
		y[i] = avg.get_avg()
	
	return y

def addSubplot(ax, data, name, index):

	x = [x['iteration'] for x in data]
	ax.plot(x, smoother([y[index] for y in data]), label=name)
	ax.legend(loc="center right")

def plot_train(data):

	fig, axs = plt.subplots(nrows=2, ncols=2)
	plt.xlabel('Iteration')
	plt.ylabel('Loss')

	fig.suptitle(os.path.basename(' Training Loss'))
		
	addSubplot(axs[0][0], data, 'BBox Loss', 'b')
	addSubplot(axs[0][1], data, 'Conf Loss', 'c')
	addSubplot(axs[1][0], data, 'Mask Loss', 'm')
	
	if data[0]['s'] is not None:
		addSubplot(axs[1][1], data, 'Segmentation Loss', 's')

	plt.show()

def plot_val(data):
	plt.title(os.path.basename(sys.argv[1]) + ' Validation mAP')
	plt.xlabel('Epoch')
	plt.ylabel('mAP')

	x = [x[1]['epoch'] for x in data if x[0]['type'] == 'box']
	plt.plot(x, [x[0]['all'] for x in data if x[0]['type'] == 'box'])
	plt.plot(x, [x[0]['all'] for x in data if x[0]['type'] == 'mask'])

	plt.legend(['BBox mAP', 'Mask mAP'])
	plt.show()

def read_val(lst):
	l = []
	for element in lst: #list	
		tupl = element.split("-")
		if not element:
			continue
		for tupleEl in tupl: #tuple
			t = []
			dct = tupleEl.split("_")
			if not tupleEl:
				continue
			for dEl in dct: #dict
				if not dEl:
					continue
				d = {}
				subd = dEl.split(",")
				for sdEl in subd: #elements of dict
					if not sdEl:
						continue
					parts = sdEl.split(':')
					try: # En el caso de type: box
						d[parts[0]] = float(parts[1])
					except:
						d[parts[0]] = parts[1]
				t.append(d)
			l.append(tuple(t))
	plot_val(l)

def read_train(lst):
	l = []
	for element in lst: #list	
		if not element:
			continue
		dct = element.split("_")
		if not element:
			continue	
		for dEl in dct: #dict
			if not dEl:
				continue
			d = {}
			subd = dEl.split(",")
			for sdEl in subd: #elements of dict
				if not sdEl:
					continue
				parts = sdEl.split(':')
				try: # En el caso de type: box
					d[parts[0]] = float(parts[1])
				except:
					d[parts[0]] = parts[1]	
			l.append(d)
	plot_train(l)

f = open("scripts/dataPlot.txt")
file = f.read()
f.close()
lst = file.split("\n")

if sys.argv[1] == 'val':
	read_val(lst)
else:
	read_train(lst)

