import re, sys, os
import matplotlib.pyplot as plt


from utils.functions import MovingAverage

with open(sys.argv[1], 'r') as f:
	inp = f.read()

patterns = {
	'train': re.compile(r'\[\s*(?P<epoch>\d+)\]\s*(?P<iteration>\d+) \|\| B: (?P<b>\S+) \| C: (?P<c>\S+) \| M: (?P<m>\S+) \|( S: (?P<s>\S+) \|)? T: (?P<t>\S+)'),
	'val': re.compile(r'\s*(?P<type>[a-z]+) \|\s*(?P<all>\S+)')
}
data = {key: [] for key in patterns}

for line in inp.split('\n'):
	for key, pattern in patterns.items():
		f = pattern.search(line)
		
		if f is not None:
			datum = f.groupdict()
			for k, v in datum.items():
				if v is not None:
					try:
						v = float(v)
					except ValueError:
						pass
					datum[k] = v
			
			if key == 'val':
				datum = (datum, data['train'][-1])
			data[key].append(datum)
			break


def smoother(y, interval=100):
	avg = MovingAverage(interval)

	for i in range(len(y)):
		avg.append(y[i])
		y[i] = avg.get_avg()
	
	return y

def addSubplot(ax, data, name, index):

	x = [x['iteration'] for x in data]
	ax.plot(x, smoother([y[index] for y in data]), label=name)
	ax.legend(loc="center right")

def plot_train(data):

	fig, axs = plt.subplots(nrows=2, ncols=2)
	plt.xlabel('Iteration')
	plt.ylabel('Loss')

	fig.suptitle(os.path.basename(' Training Loss'))
		
	addSubplot(axs[0][0], data, 'BBox Loss', 'b')
	addSubplot(axs[0][1], data, 'Conf Loss', 'c')
	addSubplot(axs[1][0], data, 'Mask Loss', 'm')
	
	if data[0]['s'] is not None:
		addSubplot(axs[1][1], data, 'Segmentation Loss', 's')

	plt.show()

def plot_val(data):
	plt.title(os.path.basename(' Validation mAP'))
	plt.xlabel('Epoch')
	plt.ylabel('mAP')

	x = [x[1]['epoch'] for x in data if x[0]['type'] == 'box']
	plt.plot(x, [x[0]['all'] for x in data if x[0]['type'] == 'box'])
	plt.plot(x, [x[0]['all'] for x in data if x[0]['type'] == 'mask'])

	plt.legend(['BBox mAP', 'Mask mAP'])
	plt.show()


if len(sys.argv) > 2 and sys.argv[2] == 'val':
	plot_val(data['val'])
else:
	plot_train(data['train'])


