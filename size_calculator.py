from convertsizes import RealSize as rs

# Parametros necesarios de computeSize
def bbox_to_coordinates(bbox):
    #[bot_left_x, bot_left_y, top_right_x - bot_left_x, top_right_y - bot_left_y] # x, y, width, height
    xMin, xMax = bbox[0], bbox[2] + bbox[0]
    yMin, yMax = bbox[1], bbox[3] + bbox[1]
    
    x = { 0: xMin, 1: xMax, 2: xMax, 3: xMin  }
    y = { 0: yMin, 1: yMin, 2: yMax, 3: yMax  }
    return x, y 


# TODO en el eval no se esta pasando el path, solo el nombre de la imagen


# Devuelve tamaño del pez a partir de los bbox pez y bandeja
def lenght_fish(bbox_fish, bbox_tray):
    # No se si es necesario pero no viene mal
    xTray, yTray = bbox_to_coordinates(bbox_tray)
    xFish, yFish = bbox_to_coordinates(bbox_fish)
    return rs.computeSize(xTray, yTray, xFish, yFish)

# Devuelve tamaño del pez a partir de los bbox pez, imagen_id y dataset (dataset.coco)
def length_fish(bbox_fish, image_id, dataset):
    image_data = dataset.imgToAnns[image_id]
    for element in image_data:
        if element['category_id'] == "Tray1" or element['category_id'] == "Tray1" :
            return lenght_fish(bbox_fish, element['bbox'])

    print("Tray not found in file ", dataset.imgs[image_id]['file_name'] )


