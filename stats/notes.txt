# Loss Key:
#  - B: Box Localization Loss
#  - C: Class Confidence Loss
#  - M: Mask Loss
#  - P: Prototype Loss
#  - D: Coefficient Diversity Loss
#  - E: Class Existence Loss
#  - S: Semantic Segmentation Loss

Training:
[  5]     170 || B: 5.666 | C: 6.144 | M: 4.854 | S: 1.325 | T: 17.989 || ETA: 28 days, 5:05:27 || timer: 99.748

[batch] iteration || B | C | M | S | T || ETA: Expected Time Arrival || timer
